# README #


## 1. Introdução ##

Este repositório contém o código fonte do componente **bancodemassa-ws-cluster** da solução **Bancodemassa - Cantina**. O componente *bancodemassa-ws-cluster* é um pacote .war que disponibiliza serviços através de WebService a solução de *Bancodemassa Cantina*.

### 2. Documentação ###

### 2.1. Diagrama de Caso de Uso (Use Case Diagram) ###

```image-file
./doc/UseCaseDiagram*.jpg
../../bancodemassa-doc/*
```

### 2.2. Diagrama de Implantação (Deploy Diagram) ###

```image-file
./doc/DeployDiagram*.jpg
../../bancodemassa-doc/*
```

### 2.3. Diagrama Modelo Banco de Dados (Database Data Model) ###

* n/a

## 3. Projeto ##

### 3.1. Pré-requisitos ###

* Linguagem de programação: Java
* IDE: Eclipse
* Apache Tomcat v8.0 para Eclipse
* JDK/JRE: 1.7 ou 1.8
* Postgresql 9.5+
* Postgresql database schemas ('bmcluster') instalado
* Postgresql database initialization/configuration scripts executados

### 3.2. Guia para Desenvolvimento ###

* Obtenha o código fonte através de um "git clone". Utilize a branch "master" se a branch "develop" não estiver disponível.
* Faça suas alterações, commit e push na branch "develop".

### 3.3. Guia para Configuração ###

Os principais itens da gestão de configuração do 'bancodemassa-ws-cluster' são:

a. Em './src/main/resources/application.properties' configure o host, porta, dbname, username e password do banco de dados Postgresql que armazena as configurações de nos de servicos do banco de massa

```properties
spring.datasource.url=jdbc:postgresql://localhost:5432/bmcluster
spring.datasource.username=postgres
spring.datasource.password=postgres
spring.datasource.driver-class-name=org.postgresql.Driver
spring.datasource.platform=postgres
```

b. Na tabela 'bm_cluster_config' do do banco de dados 'bmcluster' Postgresql deverá existir uma configuração de um no disponivel. A coluna 'url_rest_service' aponta para o endereco do webservice que vai atender as requisições do tipo 'content_type' e 'environment_type'. A coluna 'status' informa se esta linha da configuração está habilitada ('ENABLED'). Esta linha foi inserida durante a execução do script sql de inicialização localizado no componente 'bancodemassa-scripts' ./scripts/sql/cluster/init-db-01.sql. Se necessário Então faça o UPDATE de correção.
```sql
SELECT cc.url_rest_service,
       ct.cluster_content_type,
       et.cluster_environment_type,
       s.cluster_status
FROM   bm_cluster_config cc
INNER  JOIN bm_cluster_content_type ct
ON     ct.id = cc.cluster_content_type_id
INNER  JOIN bm_cluster_environment_type et
ON     et.id = cc.cluster_environment_type_id
INNER  JOIN bm_cluster_status s
ON     s.id = cc.cluster_status_id
;
 | url_rest_service                                  | content_type | environment_type | status
-+---------------------------------------------------+--------------+------------------+----------
 | http://localhost:8080/bancodemassa-ws-node/dados/ | ETL          | HML              | ENABLED
```


### 3.4. Guia para Teste ###

a. CT-01-01: Teste técnico do componente stored function do 'bancodemassa-scripts' está respondendo as requisições que serão feitas pelo componente WebService 'bancodemassa-ws-cluster'. 

Este componente  responde a uma requisição de solicitação de massa. O parâmetro de entrada é um Json com especificação de ( ClasseMassa, ContentType, EnvironmentType ). Para simplificar vamos passar um Json vazio e a resposta esperada deve ser um erro de não reconhecimento dos elementos, mas valida que o componente está funcionando.

```sql
\CONNECT bmcluster
SELECT fx_available_node('{}');
--------------------------------
{"node_return": {"db_host": "", "db_name": "", "db_port": "", "status_code": 0, "status_message": "ERROR: Unrecognized JSON element {node: {content_type:  }}", "url_rest_service": ""}}
```


b. CT-01-02: Teste basico verificar se o WebService 'bancodemassa-ws-cluster' está funcionando

Suba o servico 'bancodemassa-ws-cluster' ( Eclipse Run >> Run As >> Tomcat v.8 Server ) e verifique se ele está recebendo as solicitações.

- request:
```browser
http://localhost:8080/bancodemassa-ws-cluster/cluster/{} 
```

- response:

```json
{"node_return":{"db_host":"","db_name":"","db_port":"","status_code":0,"status_message":"ERROR: Unrecognized JSON element {node: {content_type:  }}","url_rest_service":""}}
```


c. CT-01-03: Teste Unitario JUnit 
Edite a classe 'WebServiceClusterTest' no pacote 'br.com.inmetrics.bancodemassa.ws' e configure a variável 'baseURI' conforme seu ambiente ex: "http://localhost:8080/bancodemassa-ws-cluster/cluster". Em seguida execute pelo JUnit ( Eclipse Run >>  Run As >> JUnit Test >> verificarSeOWebServiceEstaNoAr ). O teste verifica se a execução do http request retorna 200.


d. CT-01-04: Teste para obter o node para atendimento de uma requisição com os seguintes parametros: i) ambiente EnvironmentType='HML'; ii) tipo de conteúdo ContentType='ETL'; iii) classe de massa 'ETL.LOGIN'

- request:
```browser
http://localhost:8080/bancodemassa-ws-cluster/cluster/{"node": { "environment_type": "HML", "content_type": "ETL", "get": {"classe_massa": "ETL.LOGIN"} } }
```

- response:

```json
{"node":{"get":{"classe_massa":"ETL.LOGIN"},"content_type":"ETL","environment_type":"HML"},"node_return":{"db_host":"localhost","db_name":"bmnode","db_port":"5432","status_code":1,"status_message":"Id(Cluster Config): 1","url_rest_service":"http://localhost:8080/bancodemassa-ws-node/dados/"}}
```


### 3.5. Guia para Implantação ###

* Obtenha o último pacote (.war) estável gerado disponível na sub-pasta './dist'.
* Configure os arquivos de parametrizações conforme o seu ambiente (siga o Guia para Configuração)
* Copie o pacote .war para o diretório ./webapp de seu servidor de aplicação Tomcat v8



### 3.6. Guia para Demonstração ###

* n/a

## Referências ##

* n/a
