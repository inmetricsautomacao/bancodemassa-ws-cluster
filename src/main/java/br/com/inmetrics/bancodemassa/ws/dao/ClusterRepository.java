package br.com.inmetrics.bancodemassa.ws.dao;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.google.gson.JsonObject;

import br.com.inmetrics.bancodemassa.ws.utils.Constantes;
import br.com.inmetrics.bancodemassa.ws.utils.Utils;

@Repository
public class ClusterRepository implements DaoGenerico<JsonObject> {
	
	private Logger logger = Logger.getLogger(ClusterRepository.class);
	
	@Autowired JdbcTemplate jdbcTemplate;
	
	public JsonObject find(JsonObject json) {
		logger.info("Cluster (Request): " + json);
		String resultado = jdbcTemplate.queryForObject(String.format("select %s('%s')", Constantes.FX_AVAILABLE_NODE, json), String.class);
		JsonObject jsonObj = Utils.parserStringTOJson(resultado);
		logger.info("Cluster (Response): " + jsonObj);
		return jsonObj;
	}

}
