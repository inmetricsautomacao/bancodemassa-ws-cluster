package br.com.inmetrics.bancodemassa.ws.dao;

public interface DaoGenerico<T> {
	
	public T find(T json);
	
}
