package br.com.inmetrics.bancodemassa.ws.resource;

import static br.com.inmetrics.bancodemassa.ws.utils.Utils.parserStringTOJson;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.inmetrics.bancodemassa.ws.dao.ClusterRepository;

@RestController
public class ClusterService {
	
	@Autowired ClusterRepository clusterRepository;
	
	@RequestMapping(value = "/cluster/{json:.+}", method = RequestMethod.GET, headers = "Accept=application/json")
	public String buscar(@PathVariable("json") String json) {
		return clusterRepository.find(parserStringTOJson(json)).toString();
	}
}
