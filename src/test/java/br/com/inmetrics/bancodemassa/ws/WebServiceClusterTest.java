package br.com.inmetrics.bancodemassa.ws;

import static com.jayway.restassured.RestAssured.baseURI;
import static com.jayway.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

import org.junit.Ignore;
import org.junit.Test;

import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.ResponseSpecification;

public class WebServiceClusterTest {

	public WebServiceClusterTest() {
		baseURI = "http://localhost:8080/bancodemassa-ws-cluster/cluster";
	}

	@Test
	@Ignore
	public void verificarSeOWebServiceEstaNoAr() {
		ResponseSpecification resultado = given().when().then();
		assertEquals(200, resultado.get().statusCode());
	}
	
	@Test
	@Ignore
	public void deveTrazerUrlDoWebServiceDoNode() {
		final String jsonMock = getJsonModel("HML", "ETL", "EC.CONFIG.STAR");
		String resultado = given().when().get("/{json}", jsonMock).then().statusCode(200).extract().asString();
		assertEquals("http://10.82.248.107:8081/bancodemassa-ws-node", resultado);
	}
	
	@Test
	@Ignore
	public void deveTrazerStatusOkNoResponse() {
		final String jsonMock = getJsonModel("HML", "ETL", "EC.CONFIG.STAR");
		Response response = given().when().get("/{json}", jsonMock);
		assertEquals(200, response.getStatusCode());
	}
	
	/**
	 * Get Json Cluster Template
	 * @param environmentType
	 * @param contentType
	 * @param classeMassa
	 * @return
	 */
	public static String getJsonModel(String environmentType, String contentType, String classeMassa){
		StringBuilder json = new StringBuilder();
		json.append("{\"node\":{");
		json.append("\"environment_type\":\""+environmentType+"\"");
		json.append(",\"content_type\":\""+contentType+"\"");
		json.append("}");
		json.append(",\"get\":{\"classe_massa\":\""+classeMassa+"\"}");
		json.append(",\"controle\":{\"reuso\": 0}}");
		return json.toString();
	}
	
}
